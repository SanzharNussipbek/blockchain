import java.security.PublicKey;

public class TxOut {
    private PublicKey address;
    private double amount;
    private String id;


    public TxOut(PublicKey address, double amount) {
        this.address = address;
        this.amount = amount;
        this.id = Encryption.SHA256(Encryption.getStringFromKey(address) + Double.toString(amount));
    }


    public String getId() {
        return id;
    }


    public double getAmount() {
        return amount;
    }


    public PublicKey getAddress() {
        return address;
    }
}
