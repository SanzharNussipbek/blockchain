public class TxIn {
    private String txOutId;
    private int txOutIndex;
    private String signature;
    private TxOut UTXO;


    public TxIn (String txOutId, int txOutIndex, String signature) {
        this.txOutId = txOutId;
        this.txOutIndex = txOutIndex;
        this.signature = signature;
        this.UTXO = null;
    }


    public TxIn(String txOutId) {
        this.txOutId = txOutId;
        this.UTXO = null;
    }


    public TxOut getUTXO() {
        return UTXO;
    }


    public void setUTXO(TxOut UTXO) {
        this.UTXO = UTXO;
    }


    public int getTxOutIndex() {
        return txOutIndex;
    }


    public String getTxOutId() {
        return txOutId;
    }


    public String getSignature() {
        return signature;
    }
}
