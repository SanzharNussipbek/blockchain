import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class User {
	private PrivateKey privateKey;
    private PublicKey publicKey;


    public HashMap<String,TxOut> UTXOs;


   public User() {
       this.UTXOs = new HashMap<>();
	   generateKeys();
   }


   public void generateKeys() {
       try {
           KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");
           keyGen.initialize(new ECGenParameterSpec("secp256r1"), new SecureRandom());
           KeyPair keyPair = keyGen.generateKeyPair();
           this.privateKey = keyPair.getPrivate();
           this.publicKey = keyPair.getPublic();
       }catch(Exception e) {
           throw new RuntimeException(e);
       }
   }


    public double getBalance() {
        double total = 0;
        for (Map.Entry<String, TxOut> item: Main.UTXOs.entrySet()){
            TxOut UTXO = item.getValue();
            if(UTXO.getAddress() == publicKey) {
                this.UTXOs.put(UTXO.getId(),UTXO);
                total += UTXO.getAmount() ;
            }
        }
        return total;
    }

    public Transaction sendFunds(PublicKey _recipient, double amount ) {
        if(this.getBalance() < amount) {
            System.out.println("#Not Enough funds to send transaction. Transaction Discarded.");
            return null;
        }
        ArrayList<TxIn> inputs = new ArrayList<TxIn>();

        float total = 0;
        for (Map.Entry<String, TxOut> item: this.UTXOs.entrySet()){
            TxOut UTXO = item.getValue();
            total += UTXO.getAmount();
            inputs.add(new TxIn(UTXO.getId()));
            if(total > amount) break;
        }

        Transaction newTransaction = new Transaction(publicKey, _recipient , amount, inputs);
        newTransaction.generateSignature(privateKey);

        for(TxIn input: inputs){
            UTXOs.remove(input.getTxOutId());
        }

        return newTransaction;
    }


    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }
}
