import java.sql.Timestamp;
import java.time.Instant;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;


public class Blockchain {
    private ArrayList<Block> blockchain;
    private int BLOCK_GENERATION_INTERVAL = 10;
    private int DIFFICULTY_ADJUSTMENT_INTERVAL = 10;
    private int COINBASE_AMOUNT = 50;
    private int DEFAULT_DIFFICULTY = 0;
    private int height;


    public Blockchain() {
        this.blockchain = new ArrayList<>();
        this.height = blockchain.size();
    }


//    public Block generateNextBlock(String data){
//        Transaction coinbaseTx = new Transaction(generateIndex(), "");
//        Transaction[] transactions = {coinbaseTx};
//        return generateRawNextBlock(transactions);
//    }


    public Block generateRawNextBlock(Transaction[] transactions) {
        int index = generateIndex();
        long timestamp = generateTimestamp();
        int difficulty = getDifficulty();
        Block prevBlock = getLastBlock();
        String prevHash = prevBlock == null ? "" : prevBlock.getHash();
        Block newBlock = findBlock(index, prevHash, timestamp, transactions.toString(), difficulty);
        add(newBlock);
        return newBlock;
    }


    public void add(Block newBlock) {
        blockchain.add(newBlock);
        this.height = blockchain.size();
    }


    private int generateIndex() {
        return this.blockchain.size();
    }


    public int getSize() {
        return this.blockchain.size();
    }


    public int getHeight() { return this.height; }


    private long generateTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }


    public void addBlock(Block block) {
        this.blockchain.add(block);
    }


    public Block getBlock(int index) {
        return this.blockchain.get(index);
    }


    private Block getLastBlock() {
        int size = this.blockchain.size();
        return size == 0 ? null : this.blockchain.get(size - 1);
    }


    public String calculateHash(int index, long timestamp, String prevHash, String data, int difficulty, int nonce) {
        return Encryption.SHA256(index + Long.toString(timestamp) + prevHash + data);
    }


    public String calculateHash(Block block) {
        return block.calculateHash();
    }


    public Block findBlock(int index, String prevHash, long timestamp, String data, int difficulty) {
        int nonce = 0;
        while (true) {
            String hash = calculateHash(index, timestamp, prevHash, data, difficulty, nonce);
            if (hashMatchesDifficulty(hash, difficulty)) {
                System.out.print("Block Mined: hash = " + hash + "\n");
                return new Block(index, timestamp, hash, prevHash, data, difficulty, nonce);
            }
            nonce++;
        }
    }


    public int getDifficulty() {
        Block lastBlock = this.getLastBlock();
        if (lastBlock == null) return DEFAULT_DIFFICULTY;
        if (lastBlock.getIndex() % DIFFICULTY_ADJUSTMENT_INTERVAL == 0 && lastBlock.getIndex() != 0) {
            return getAdjustedDifficulty(lastBlock);
        }
        return lastBlock.getDifficulty();
    }


    public int getAdjustedDifficulty(Block lastBlock) {
        Block prevAdjustmentBlock = this.blockchain.get(this.getSize() - DIFFICULTY_ADJUSTMENT_INTERVAL);
        long timeExpected = BLOCK_GENERATION_INTERVAL * DIFFICULTY_ADJUSTMENT_INTERVAL;
        long timeTaken = lastBlock.getTimestamp() - prevAdjustmentBlock.getTimestamp();
        if (timeTaken < timeExpected / 2) {
            return  prevAdjustmentBlock.getDifficulty() + 1;
        }
        if (timeTaken > timeExpected * 2) {
            return prevAdjustmentBlock.getDifficulty() - 1;
        }
        return prevAdjustmentBlock.getDifficulty();
    }


    public boolean hashMatchesDifficulty(String hash, int difficulty) {
        String hashBinary = hexToBin(hash);
        String requiredPrefix = repeat('0', difficulty);
        return hashBinary.startsWith(requiredPrefix);
    }


    private String repeat(char c, int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += c;
        }
        return result;
    }


    private String hexToBin(String s) {
        return new BigInteger(s, 16).toString(2);
    }


    public boolean isValidChain() {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = new String(new char[DEFAULT_DIFFICULTY]).replace('\0', '0');
        HashMap<String,TxOut> tempUTXOs = new HashMap<>();
        Transaction genesisTransaction = this.getBlock(0).getTransactions().get(0);
        tempUTXOs.put(genesisTransaction.getTxOuts().get(0).getId(), genesisTransaction.getTxOuts().get(0));

        for(int i = 1; i < this.getSize(); i++) {

            currentBlock = this.getBlock(i);
            previousBlock = this.getBlock(i - 1);

            if(!currentBlock.getHash().equals(currentBlock.calculateHash()) ){
                System.out.println("#Current Hashes not equal [" + i + "]: " + currentBlock.getHash() + " | " + currentBlock.calculateHash());
                return false;
            }

            if(!previousBlock.getHash().equals(currentBlock.getPrevHash())) {
                System.out.println("#Previous Hashes not equal");
                return false;
            }

            if(!currentBlock.getHash().substring( 0, DEFAULT_DIFFICULTY).equals(hashTarget)) {
                System.out.println("#This block hasn't been mined");
                return false;
            }

            TxOut tempOutput;
            for(int t = 0; t < currentBlock.getTransactions().size(); t++) {
                Transaction currentTransaction = currentBlock.getTransactions().get(t);

                if(!currentTransaction.verifySignature()) {
                    System.out.println("#Signature on Transaction(" + t + ") is Invalid");
                    return false;
                }
                if(currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
                    System.out.println("#Inputs are note equal to outputs on Transaction(" + t + ")");
                    return false;
                }

                for(TxIn input: currentTransaction.getTxIns()) {
                    tempOutput = tempUTXOs.get(input.getTxOutId());

                    if(tempOutput == null) {
                        System.out.println("#Referenced input on Transaction(" + t + ") is Missing");
                        return false;
                    }

                    if(input.getUTXO().getAmount() != tempOutput.getAmount()) {
                        System.out.println("#Referenced input Transaction(" + t + ") value is Invalid");
                        return false;
                    }

                    tempUTXOs.remove(input.getTxOutId());
                }

                for(TxOut output: currentTransaction.getTxOuts()) {
                    tempUTXOs.put(output.getId(), output);
                }

                if( currentTransaction.getTxOuts().get(0).getAddress() != currentTransaction.getAddress()) {
                    System.out.println("#Transaction(" + t + ") output reciepient is not who it should be");
                    return false;
                }
                if( currentTransaction.getTxOuts().get(1).getAddress() != currentTransaction.getSender()) {
                    System.out.println("#Transaction(" + t + ") output 'change' is not sender.");
                    return false;
                }

            }

        }
        System.out.println("\nBlockchain is valid");
        return true;
    }
}
