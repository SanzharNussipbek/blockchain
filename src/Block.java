import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;

public class Block {
    private int index;
    private long timestamp;
    private String hash;
    private String prevHash;
    private String data;
    private int difficulty;
    private int nonce;
    private ArrayList<Transaction> transactions;


    public Block(int index, long timestamp, String hash, String prevHash, String data, int difficulty, int nonce) {
        this.index = index;
        this.timestamp = timestamp;
        this.hash = hash;
        this.prevHash = prevHash;
        this.data = data;
        this.difficulty = difficulty;
        this.nonce = nonce;
        this.transactions = new ArrayList<>();
    }


    public Block(String prevHash) {
        this.prevHash = prevHash;
        this.timestamp = generateTimestamp();
        this.data = "";
        this.hash = calculateHash();
        this.transactions = new ArrayList<>();
    }


    private long generateTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }


    public boolean isValidBlock(Block previousBlock) {
        return (previousBlock.getIndex() + 1 == this.getIndex())
                && (previousBlock.getHash().equals(this.getPrevHash()))
                && (this.calculateHash().equals(this.getHash()));
    }


    public String calculateHash() {
        return Encryption.SHA256(this.timestamp + this.prevHash + this.nonce + this.data);
    }


    public void mineBlock(int difficulty) {
        String target = new String(new char[difficulty]).replace('\0', '0');
        while(!this.getHash().substring( 0, difficulty).equals(target)) {
            this.setNonce(this.getNonce() + 1);
            this.setHash(calculateHash());
        }
        System.out.println("Block Mined! Hash = " + this.getHash());
    }


    @Override
    public String toString() {
        return "Block{" +
                "index=" + index +
                ", timestamp=" + timestamp +
                ", hash='" + hash + '\'' +
                ", prevHash='" + prevHash + '\'' +
                ", data='" + data + '\'' +
                ", difficulty=" + difficulty +
                ", nonce=" + nonce +
                ", transactions=" + transactions +
                '}';
    }


    public boolean addTransaction(Transaction tx) {
        if(tx == null) return false;

        if((!"0".equals(this.prevHash))) {
            if((tx.processTransaction() != true)) {
                System.out.println("ERROR: Failed to add transaction to the block.");
                return false;
            }
        }

        this.transactions.add(tx);
        System.out.println("SUCCESS: Transaction added to Block");
        return true;
    }


    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }


    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }


    public void setHash(String hash) {
        this.hash = hash;
    }


    public void setNonce(int nonce) {
        this.nonce = nonce;
    }


    public int getIndex() {
        return this.index;
    }


    public long getTimestamp() {
        return this.timestamp;
    }


    public String getHash() {
        return this.hash;
    }


    public String getPrevHash() {
        return this.prevHash;
    }


    public String getData() {
        return this.data;
    }


    public int getDifficulty() {
        return this.difficulty;
    }


    public int getNonce() {
        return this.nonce;
    }

}
