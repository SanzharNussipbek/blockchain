import java.security.Security;
import java.util.HashMap;

import com.google.gson.GsonBuilder;

public class Main {
    public static HashMap<String,TxOut> UTXOs = new HashMap<>();
    public Transaction genesisTransaction;
    public Blockchain blockchain;
    private int DEFAULT_DIFFICULTY = 0;
    private double COINBASE_AMOUNT = 50;

    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        blockchain = new Blockchain();

        User userA = new User();
        User userB = new User();
        User coinbase = new User();

        genesisTransaction = new Transaction(coinbase.getPublicKey(), userA.getPublicKey(), COINBASE_AMOUNT, null);
        genesisTransaction.generateSignature(coinbase.getPrivateKey());
        genesisTransaction.setId("0");
        genesisTransaction.addTxOut(new TxOut(genesisTransaction.getAddress(), genesisTransaction.getAmount())); //manually add the Transactions Output
        UTXOs.put(genesisTransaction.getTxOuts().get(0).getId(), genesisTransaction.getTxOuts().get(0));

        System.out.println("Creating and Mining Genesis block... ");
        Block genesis = new Block("0");
        genesis.addTransaction(genesisTransaction);
        addBlock(genesis);


        Block block1 = new Block(genesis.getHash());
        System.out.println("\nUserA Balance: " + userA.getBalance());
        System.out.println("\nTransaction from UserA to UserB (amount = 20)");
        block1.addTransaction(userA.sendFunds(userB.getPublicKey(), 20));
        addBlock(block1);


        System.out.println("\nUserA Balance: " + userA.getBalance());
        System.out.println("UserB Balance: " + userB.getBalance());

        Block block2 = new Block(block1.getHash());
        System.out.println("\nTransaction from UserA to UserB (amount = 50)");
        block2.addTransaction(userA.sendFunds(userB.getPublicKey(), 50));
        addBlock(block2);

        System.out.println("\nUserA Balance: " + userA.getBalance());
        System.out.println("UserB Balance: " + userB.getBalance());

        Block block3 = new Block(block2.getHash());
        System.out.println("\nTransaction from UserB to UserA (amount = 10)");
        block3.addTransaction(userB.sendFunds( userA.getPublicKey(), 10));
        System.out.println("\nUserA Balance: " + userA.getBalance());
        System.out.println("UserB Balance: " + userB.getBalance());

        blockchain.isValidChain();
    }

    public void addBlock(Block block) {
        block.mineBlock(DEFAULT_DIFFICULTY);
        blockchain.addBlock(block);
    }

    public Boolean isChainValid(Blockchain blockchain) {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = new String(new char[DEFAULT_DIFFICULTY]).replace('\0', '0');
        HashMap<String,TxOut> tempUTXOs = new HashMap<>();
        tempUTXOs.put(genesisTransaction.getTxOuts().get(0).getId(), genesisTransaction.getTxOuts().get(0));

        for(int i = 1; i < blockchain.getSize(); i++) {

            currentBlock = blockchain.getBlock(i);
            previousBlock = blockchain.getBlock(i - 1);

            if(!currentBlock.getHash().equals(currentBlock.calculateHash()) ){
                System.out.println("#Current Hashes not equal [" + i + "]: " + currentBlock.getHash() + " | " + currentBlock.calculateHash());
                return false;
            }

            if(!previousBlock.getHash().equals(currentBlock.getPrevHash())) {
                System.out.println("#Previous Hashes not equal");
                return false;
            }

            if(!currentBlock.getHash().substring( 0, DEFAULT_DIFFICULTY).equals(hashTarget)) {
                System.out.println("#This block hasn't been mined");
                return false;
            }

            TxOut tempOutput;
            for(int t = 0; t < currentBlock.getTransactions().size(); t++) {
                Transaction currentTransaction = currentBlock.getTransactions().get(t);

                if(!currentTransaction.verifySignature()) {
                    System.out.println("#Signature on Transaction(" + t + ") is Invalid");
                    return false;
                }
                if(currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
                    System.out.println("#Inputs are note equal to outputs on Transaction(" + t + ")");
                    return false;
                }

                for(TxIn input: currentTransaction.getTxIns()) {
                    tempOutput = tempUTXOs.get(input.getTxOutId());

                    if(tempOutput == null) {
                        System.out.println("#Referenced input on Transaction(" + t + ") is Missing");
                        return false;
                    }

                    if(input.getUTXO().getAmount() != tempOutput.getAmount()) {
                        System.out.println("#Referenced input Transaction(" + t + ") value is Invalid");
                        return false;
                    }

                    tempUTXOs.remove(input.getTxOutId());
                }

                for(TxOut output: currentTransaction.getTxOuts()) {
                    tempUTXOs.put(output.getId(), output);
                }

                if( currentTransaction.getTxOuts().get(0).getAddress() != currentTransaction.getAddress()) {
                    System.out.println("#Transaction(" + t + ") output reciepient is not who it should be");
                    return false;
                }
                if( currentTransaction.getTxOuts().get(1).getAddress() != currentTransaction.getSender()) {
                    System.out.println("#Transaction(" + t + ") output 'change' is not sender.");
                    return false;
                }

            }

        }
        System.out.println("Blockchain is valid");
        return true;
    }
}
