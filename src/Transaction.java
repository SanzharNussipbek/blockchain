import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;

public class Transaction {
    private String id;
    private ArrayList<TxIn> txIns;
    private ArrayList<TxOut> txOuts;
    private PublicKey sender;
    private PublicKey address;
    private double amount;
    private byte[] signature;
    private int sequence = 0;

    private double MINIMUM_TRANSACTION = 0.1;


    public Transaction(PublicKey from, PublicKey to, double amount,  ArrayList<TxIn> txIns) {
        this.sender = from;
        this.address = to;
        this.amount = amount;
        this.txIns = txIns == null ? new ArrayList<>() : txIns;
        this.txOuts = new ArrayList<>();
        this.id = getTransactionId(this.txIns, this.txOuts);
    }


    private String calulateHash() {
        this.sequence++;
        return Encryption.SHA256(Encryption.getStringFromKey(this.sender)
                                    + Encryption.getStringFromKey(this.address)
                                    + Double.toString(this.amount)
                                    + this.sequence
        );
    }


    public void addTxOut(TxOut txOut) {
        this.txOuts.add(txOut);
    }


    public boolean processTransaction() {
        if(verifySignature() == false) {
            System.out.println("ERROR: Failed to verify transaction signature");
            return false;
        }

        for(TxIn txIn : this.txIns) {
            txIn.setUTXO(Main.UTXOs.get(txIn.getTxOutId()));
        }

        if(getInputsValue() < MINIMUM_TRANSACTION) {
            System.out.println("ERROR: Transaction amount insufficient: " + getInputsValue());
            System.out.println("Please enter the amount greater than " + MINIMUM_TRANSACTION);
            return false;
        }

        double leftOver = getInputsValue() - this.amount;
        this.id = calulateHash();
        this.txOuts.add(new TxOut( this.address, this.amount));
        this.txOuts.add(new TxOut( this.sender, leftOver));

        for(TxOut txOut : this.txOuts) {
            Main.UTXOs.put(txOut.getId() , txOut);
        }

        for(TxIn txIn : this.txIns) {
            if(txIn.getUTXO() == null) continue;
            Main.UTXOs.remove(txIn.getUTXO().getId());
        }

        return true;
    }


    public void generateSignature(PrivateKey privateKey) {
        this.signature = Encryption.applyECDSASig(privateKey,Encryption.getStringFromKey(this.sender)
                                                                + Encryption.getStringFromKey(this.address)
                                                                + Double.toString(this.amount));
    }


    public boolean verifySignature() {
        return Encryption.verifyECDSASig(this.sender, Encryption.getStringFromKey(this.sender)
                                                        + Encryption.getStringFromKey(this.address)
                                                        + Double.toString(this.amount), this.signature);
    }


    public float getInputsValue() {
        float total = 0;
        for(TxIn txIn : this.txIns) {
            if(txIn.getUTXO() == null) continue;
            total += txIn.getUTXO().getAmount();
        }
        return total;
    }


    public float getOutputsValue() {
        float total = 0;
        for(TxOut txOut : this.txOuts) {
            total += txOut.getAmount();
        }
        return total;
    }


    public String getTransactionId(ArrayList<TxIn> txIns, ArrayList<TxOut> txOuts) {
        String txInContent = getTxInContent(txIns);
        String txOutContent = getTxOutContent(txOuts);
        return Encryption.SHA256(txInContent + txOutContent);
    }


    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", txIns=" + txIns +
                ", txOuts=" + txOuts +
                ", sender=" + sender +
                ", address=" + address +
                ", amount=" + amount +
                ", signature=" + Arrays.toString(signature) +
                ", sequence=" + sequence +
                '}';
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getId() {
        return id;
    }


    public ArrayList<TxIn> getTxIns() {
        return txIns;
    }


    public ArrayList<TxOut> getTxOuts() {
        return txOuts;
    }


    public double getAmount() {
        return amount;
    }


    public PublicKey getAddress() {
        return address;
    }


    public PublicKey getSender() {
        return sender;
    }


    private String getTxInContent(ArrayList<TxIn> txIns) {
        String result = "";
        for (TxIn txIn : txIns) {
            result += txIn.getTxOutId() + txIn.getTxOutIndex();
        }
        return result;
    }


    private String getTxOutContent(ArrayList<TxOut> txOuts) {
        String result = "";
        for (TxOut txOut : txOuts) {
            result += txOut.getAddress().toString() + txOut.getAmount();
        }
        return result;
    }
}
